package com.example.testbot.assets;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.testbot.helpers.Regex;

import java.util.Objects;

/*
    Holds resources information of a village: production, capacity, storage
 */
public class VillageResources implements Parcelable {
    public Resource lumber;
    public Resource clay;
    public Resource iron;
    public Resource crop;

    private int mWarehouseCapacity;
    private int mGranaryCapacity;

    public VillageResources() {
        lumber = new Resource();
        clay = new Resource();
        iron = new Resource();
        crop = new Resource();
    }

    public VillageResources(Parcel in) {
        lumber = (Resource) in.readValue(Resource.class.getClassLoader());
        clay = (Resource) in.readValue(Resource.class.getClassLoader());
        iron = (Resource) in.readValue(Resource.class.getClassLoader());
        crop = (Resource) in.readValue(Resource.class.getClassLoader());
    }


    /**
     * Designed to update storage each second. Use with care since it will potentially cause bugs
     */
    public void timerUpdate() {
        lumber.timerUpdateStorage();
        clay.timerUpdateStorage();
        iron.timerUpdateStorage();
        crop.timerUpdateStorage();
    }


    /**
     * Get resources production from the formatted js script and set them
     * @param jsScript js script contains all resources information. See ResourcesRepository.updateResources()
     */
    public void updateProduction(String jsScript) {
        try {
            int lumber = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l1\": )(\\d+?)(?:,)", jsScript)));

            int clay   = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l2\": )(\\d+?)(?:,)", jsScript)));

            int iron   = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l3\": )(\\d+?)(?:,)", jsScript)));

            int crop   = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l4\": )(\\d+?)(?:,)", jsScript)));

            this.setProduction(lumber, clay, iron, crop);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    /**
     * Get resources storage from the formatted js script and set them
     * @param jsScript js script contains all resources information. See ResourcesRepository.updateResources()
     */
    public void updateStorage(String jsScript) {
        try {
            int lumber = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l1\": )(\\d+?)(?:,)", jsScript)));

            int clay   = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l2\": )(\\d+?)(?:,)", jsScript)));

            int iron   = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l3\": )(\\d+?)(?:,)", jsScript)));

            int crop   = Integer.parseInt(
                            Objects.requireNonNull(
                                Regex.findFirst("(?:\"l4\": )(\\d+?)(?:\\})", jsScript)));

            this.setStorage(lumber, clay, iron, crop);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    /**
     * Get storage capacity from the formatted js script and set them
     * @param jsScript js script contains all resources information. See ResourcesRepository.updateResources()
     */
    public void updateCapacity(String jsScript) {
        try {
            int warehouse = Integer.parseInt(
                Objects.requireNonNull(
                    Regex.findFirst("(?:\"l1\": )(\\d+?)(?:,)", jsScript)));

            int granary = Integer.parseInt(
                Objects.requireNonNull(
                    Regex.findFirst("(?:\"l4\": )(\\d+?)(?:\\})", jsScript)));

            this.setCapacity(warehouse, granary);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    /**
     * Set storage for all 4 types of resources.
     * If need to set storage for each one, set its attribute directly (e.g: lumber.storage = x)
     *
     * @param lumber lumber storage
     * @param clay clay storage
     * @param iron iron storage
     * @param crop crop storage
     */
    private void setStorage(int lumber, int clay, int iron, int crop) {
//        this.lumber.storage = lumber;
//        this.clay.storage = clay;
//        this.iron.storage = iron;
//        this.crop.storage = crop;
        this.lumber.setStorage(lumber);
        this.clay.setStorage(clay);
        this.iron.setStorage(iron);
        this.crop.setStorage(crop);
    }


    /**
     * Set production for all 4 types of resources
     * If need to set for each one, set its attribute directly (e.g: lumber.production = x)
     *
     * @param lumber lumber production
     * @param clay clay production
     * @param iron iron production
     * @param crop crop production
     */
    private void setProduction(int lumber, int clay, int iron, int crop) {
        this.lumber.productionPerHour = lumber;
        this.clay.productionPerHour = clay;
        this.iron.productionPerHour = iron;
        this.crop.productionPerHour = crop;
    }


    /**
     * Set capacity for all 4 types of resources
     *
     * @param warehouse warehouse capacity
     * @param granary granary capacity* 300
     */
    private void setCapacity(int warehouse, int granary) {
        mWarehouseCapacity = warehouse;
        mGranaryCapacity = granary;

        lumber.capacity = mWarehouseCapacity;
        clay.capacity = mWarehouseCapacity;
        iron.capacity = mWarehouseCapacity;
        crop.capacity = mGranaryCapacity;
    }


    /**
     * Set warehouse capacity with builder design. Need to call set() afterwards
     * e.g :village.warehouseCapacity(1000).set()
     *
     * @param capacity this village's warehouse capacity
     */
    public void warehouseCapacity(int capacity) {
        mWarehouseCapacity = capacity;
    }


    /**
     * Set warehouse capacity with builder design. Need to call set() afterwards
     * e.g :village.granaryCapacity(1000).set()
     *
     * @param capacity this village's granary capacity
     */
    public void granaryCapacity(int capacity) {
        mGranaryCapacity = capacity;
    }


    /**
     * Set warehouse AND/OR granary capacity with builder design.
     * Need to be called after 2 above methods
     */
    public void set() {
        setCapacity(mWarehouseCapacity, mGranaryCapacity);
    }


    @Override
    public String toString() {
        return String.format("Lumber:\n%s\n\nClay:\n%s\n\nIron:\n%s\n\nCrop:\n%s", lumber, clay, iron, crop);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lumber);
        dest.writeValue(clay);
        dest.writeValue(iron);
        dest.writeValue(crop);
        dest.writeInt(mWarehouseCapacity);
        dest.writeInt(mGranaryCapacity);
    }

    public static final Parcelable.Creator<VillageResources> CREATOR =
        new Parcelable.Creator<VillageResources>() {
            @Override
            public VillageResources createFromParcel(Parcel in) {
                return new VillageResources(in);
            }

            @Override
            public VillageResources[] newArray(int size) {
                return new VillageResources[size];
            }
    };

}
