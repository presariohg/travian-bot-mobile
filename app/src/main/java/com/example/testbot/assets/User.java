package com.example.testbot.assets;

import com.example.testbot.Constants.Tribe;

import java.util.Map;



public class User {
    public static Tribe tribe;
    public static String id;
    public static String password;
    public static String serverUrl;
    public static Map<String, String> cookies;
    public static boolean isShowingUpgradeIndicator = true;

    /**
     * Setup login credentials for this session
     * @param id User's login id
     * @param password User's login password
     * @param serverUrl Server's url
     */
    public static void setCredentials(String id, String password, String serverUrl) {
        User.id = id;
        User.password = password;
        User.serverUrl = serverUrl;
    }


    @Override
    public String toString() {
        return String.format("%s | %s", id, password);
    }
}
