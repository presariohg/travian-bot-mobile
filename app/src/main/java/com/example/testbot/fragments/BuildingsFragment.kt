package com.example.testbot.fragments

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.testbot.Constants
import com.example.testbot.adapters.BuildingsRecyclerViewAdapter
import com.example.testbot.assets.Building

open class BuildingsFragment : Fragment() {

    protected var mListener: OnFragmentInteractionListener? = null

    /**
     * Attach this Fragment to its master Activity. Also check if its owner implemented
     * [OnFragmentInteractionListener] or not (it must)
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    fun updateBuildingList(buildings: ArrayList<Building>) {
        with (view as RecyclerView) {
            (adapter as BuildingsRecyclerViewAdapter).updateBuildingList(buildings)
            adapter!!.notifyDataSetChanged()
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onButtonClicked(index : Int, flag: Constants.FragmentFlags)
    }
}