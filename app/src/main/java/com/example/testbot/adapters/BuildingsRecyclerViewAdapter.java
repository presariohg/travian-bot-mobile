package com.example.testbot.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testbot.R;
import com.example.testbot.assets.Building;
import com.example.testbot.helpers.Listener;

import java.util.ArrayList;

public class BuildingsRecyclerViewAdapter extends RecyclerView.Adapter<BuildingsRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Building> mBuildings;
    private Listener mListener;

    /**
     * Initialize the adapter for this recycler view
     * @param context Context contains this application's info to get the correct image source
     * @param listener OnClickListener for the elements in each item
     */
    public BuildingsRecyclerViewAdapter(Context context, Listener listener) {
        mContext = context;
        mListener = listener;

        mBuildings = new ArrayList<>();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView buildingName;
        TextView buildingLevel;
        Button btAdd;
        RelativeLayout backgroundLayout;

        ViewHolder(View itemView) {
            super(itemView);
            btAdd = itemView.findViewById(R.id.bt_add);
            image = itemView.findViewById(R.id.iv_building_image);
            buildingName = itemView.findViewById(R.id.tv_building_name);
            buildingLevel = itemView.findViewById(R.id.tv_building_level);
            backgroundLayout = itemView.findViewById(R.id.layout_background);

            // Bind listener with button
            btAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onButtonClicked(v, getAdapterPosition());
                }
            });

            // Bind listener with layout
            backgroundLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, getAdapterPosition());
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int index) {
        View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_buildings, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int index) {
        Building building = mBuildings.get(index);

        if (building.isBuildingSite) {
            viewHolder.buildingName.setText(building.name);
            viewHolder.image.setVisibility(View.INVISIBLE);
            viewHolder.buildingLevel.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.image.setImageResource(building.getImgSource(mContext));
            viewHolder.image.setVisibility(View.VISIBLE);

            String levelText = " " + building.level + " ";
            viewHolder.buildingLevel.setText(levelText);
            viewHolder.buildingLevel.setVisibility(View.VISIBLE);
            viewHolder.buildingName.setText(building.name);
        }

    }

    @Override
    public int getItemCount() {
        return mBuildings.size();
    }

    public void updateBuildingList(ArrayList<Building> buildings) {
        this.mBuildings = buildings;
    }
}
