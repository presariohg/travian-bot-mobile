package com.example.testbot.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.testbot.Constants.SECONDS_PER_HOUR
import com.example.testbot.assets.*
import com.example.testbot.helpers.Regex
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.lang.IndexOutOfBoundsException

class BuildingUpgradeRepository {
    @get:JvmName("latestPage")
    val latestPage : MutableLiveData<Document> = MutableLiveData()


    @set:JvmName("setVillageResource")
    var village : VillageResources? = null

    @set:JvmName("setUpgradingQueue")
    var buildingQueue : BuildingQueue = BuildingQueue()

    @get:JvmName("getWaitTime")
    @set:JvmName("setWaitTime")
    var waitingTime = 0


    /**
     * If not refreshed, check next building's wait time again. Only upgrade building if already
     * recently refreshed
     */
    @get:JvmName("isRefreshed")
    @set:JvmName("setRefreshed")
    var isRefreshed = false

    @get:JvmName("alreadyRequesting")
    @set:JvmName("setRequesting")
    var alreadyRequesting = false


    fun timerUpdate() {
        waitingTime--
        Log.i("Timer", "BuildingUpgradeRepo timer: $waitingTime")
    }


    /**
     * Calculate how much time would be needed for this [building] to meet its resources requirements
     */
    fun updateWaitingTime(building: Building) {
        doAsync {
            val doc : Document = Jsoup.connect(building.url)
                                    .cookies(User.cookies)
                                    .get()
            latestPage.postValue(doc)

            val requirements : HashMap<String, Int> = HashMap()

            // Get resource requirements
            requirements["lumber"] = doc.getElementsByClass("r1Big")[0].siblingElements()[0].text().toInt()
            requirements["clay"]   = doc.getElementsByClass("r2Big")[0].siblingElements()[0].text().toInt()
            requirements["iron"]   = doc.getElementsByClass("r3Big")[0].siblingElements()[0].text().toInt()
            requirements["crop"]   = doc.getElementsByClass("r4Big")[0].siblingElements()[0].text().toInt()

            // Get current resources info
            village = ResourcesRepository.updateResources(doc)

            /**
             * Calculate wait time with given requirement and resource info
             */
            fun waitTime(requirement : Int, resource: Resource) : Int {
                val resourceLeft : Double = resource.storage - requirement
//                Log.i("debug", "require: $requirement, got: ${resource}")
                if (resourceLeft >= 0) {
                    return 0
                } else {
                    val productionPerSec : Double = resource.productionPerHour.toDouble() / SECONDS_PER_HOUR
                    return ((requirement - resource.storage) / productionPerSec).toInt()
                }
            }

            val waitTimes = IntArray(4)

            waitTimes[0] = waitTime(requirements["lumber"]!!, village!!.lumber)
            waitTimes[1] = waitTime(requirements["clay"]!!, village!!.clay)
            waitTimes[2] = waitTime(requirements["iron"]!!, village!!.iron)
            waitTimes[3] = waitTime(requirements["crop"]!!, village!!.crop)

            val result = waitTimes.max()!!

            this.weakRef.get()!!.isRefreshed = (result <= 0)

            // Counter and timing are may not be exact by seconds so an additional of 5 secs would be helpful
            this.weakRef.get()!!.waitingTime = if (result == 0) 0 else (result + 5)
        }
    }


    /**
     * Upgrade a [building] after meeting its requirements
     */
    fun upgrade(building : Building) {
        doAsync {

            this.weakRef.get()!!.alreadyRequesting = true

            var doc : Document = Jsoup.connect(building.url)
                                      .cookies(User.cookies)
                                      .get()

            val rawString : String
            try {
                rawString = doc.getElementsByClass("section1")[0]
                        .getElementsByClass("green build")[0]
                        .attr("onclick")

            } catch (e : IndexOutOfBoundsException) {
                // skip if not meet requirements
                Log.i("upgrade", "Upgrading Building $building skipped, requirements did not meet")
                return@doAsync
            }

            val upgradeUrl = Regex.findFirst("(?:window.location.href = ')(.+?)(?:'; return false)", rawString)
            doc = Jsoup.connect(User.serverUrl + upgradeUrl)
                       .cookies(User.cookies)
                       .get()
            /*
             Other requests can be run now. Note that this method to prevent parallel requests may
             not be effective in extremely rare cases, since the upgradings list will only be updated
             after this task finishes. It can only happen if the 1s-period timer in MainViewModel
             goes off right after the line below and right before the upgradings list is updated.
             Extremely rare in my opinion, but still possible, so it can potentially cause bugs
             */
            this.weakRef.get()!!.alreadyRequesting = false
            Log.i("upgrade", "Building $building upgraded")

            latestPage.postValue(doc)
        }
    }
}