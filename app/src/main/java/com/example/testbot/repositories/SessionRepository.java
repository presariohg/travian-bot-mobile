package com.example.testbot.repositories;


import android.os.AsyncTask;
import android.util.Log;

import com.example.testbot.assets.User;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import static com.example.testbot.Constants.Tribe.Egyptians;
import static com.example.testbot.Constants.Tribe.Gauls;
import static com.example.testbot.Constants.Tribe.Huns;
import static com.example.testbot.Constants.Tribe.Natars;
import static com.example.testbot.Constants.Tribe.Romans;
import static com.example.testbot.Constants.Tribe.Teutons;
import static com.example.testbot.Constants.TribeCssClass;

public class SessionRepository {

    public static void login() {
        new AsyncTask<Void, Void, Void>() {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                // login to server using received credentials
                Connection.Response res = Jsoup.connect(User.serverUrl)
                        .data("name", User.id)
                        .data("password", User.password)
                        .data("s1", "Login")
                        .data("w", "1920:1080")
                        .data("login", "1566983386")
                        .followRedirects(true)
                        .method(Connection.Method.POST)
                        .execute();

                res.bufferUp();
                // save retrieved cookies
                User.cookies = res.cookies();

                findUserTribe(res.parse());

                // TODO: Catch the case in which logging in failed, retrieved only one cookie entry
            } catch (IOException e) {
                Log.i("debug", "IOException @SessionRepository/login");
            }

            return null;
        }
        }.execute();
    }


    private static void findUserTribe(Document doc) {
        if (checkTribe(doc, TribeCssClass.ROMANS))
            User.tribe = Romans;

        else if (checkTribe(doc, TribeCssClass.TEUTONS))
            User.tribe = Teutons;

        else if (checkTribe(doc, TribeCssClass.GAULS))
            User.tribe = Gauls;

        else if (checkTribe(doc, TribeCssClass.NATARS))
            User.tribe = Natars;

        else if (checkTribe(doc, TribeCssClass.EGYPTIANS))
            User.tribe = Egyptians;

        else if (checkTribe(doc, TribeCssClass.HUNS))
            User.tribe = Huns;
    }


    private static boolean checkTribe(Document doc, String tribeClass) {
        return doc.getElementsByClass(tribeClass).size() == 1;
    }
}
