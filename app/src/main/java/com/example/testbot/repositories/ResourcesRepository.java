package com.example.testbot.repositories;

import com.example.testbot.assets.VillageResources;
import com.example.testbot.helpers.Regex;

import org.jsoup.nodes.Document;

public class ResourcesRepository {
    private static ResourcesRepository instance;

    public static ResourcesRepository getInstance() {
        if (instance == null) {
            instance = new ResourcesRepository();
        }

        return instance;
    }


    /**
     * Update current village resource info through a newly got Jsoup document
     * @param page Jsoup {@link Document} contains resources info
     * @return A {@link VillageResources} objects contains new resources info
     */
    public static VillageResources updateResources(Document page) {
        VillageResources resources = new VillageResources();

        // js script contains all data we want: resources production, storage and capacity
        String jsScript = page.getElementById("contentOuterContainer").
                               getElementsByTag("script").
                               get(0).
                               html();
        /*
            The caught string should look like this:

            var resources = {
                production: {"l1": 174,"l2": 156,"l3": 144,"l4": 183,"l5": 75},
                storage: {"l1": 471,"l2": 463,"l3": 457,"l4": 476},
                maxStorage: {"l1": 800,"l2": 800,"l3": 800,"l4": 1200}
            };
         */

        String capacity = Regex.findFirst("(maxStorage:.+?\\})", jsScript);
        resources.updateCapacity(capacity);

        String production = Regex.findFirst("(production:.+?\\n)", jsScript);
        resources.updateProduction(production);

        String storage = Regex.findFirst("(storage:.+?\\n)", jsScript);
        resources.updateStorage(storage);

        return resources;
    }
}
