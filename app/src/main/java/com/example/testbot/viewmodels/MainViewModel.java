package com.example.testbot.viewmodels;

import android.app.Application;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.example.testbot.Constants.Tribe;
import com.example.testbot.assets.Building;
import com.example.testbot.assets.BuildingQueue;
import com.example.testbot.assets.UpgradingBuilding;
import com.example.testbot.assets.User;
import com.example.testbot.assets.VillageResources;
import com.example.testbot.repositories.AvailableNewBuildingsRepository;
import com.example.testbot.repositories.BuildingUpgradeRepository;
import com.example.testbot.repositories.BuildingsRepository;
import com.example.testbot.repositories.ResourcesRepository;
import com.example.testbot.repositories.SessionRepository;
import com.example.testbot.repositories.UpgradingRepository;

import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Queue;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

public class MainViewModel extends AndroidViewModel {

    private MutableLiveData<Document> mLatestPage = new MutableLiveData<>();
    private MutableLiveData<VillageResources> mVillageResources = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Building>> mOuterBuildings = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Building>> mInnerBuildings = new MutableLiveData<>();
    private MutableLiveData<ArrayList<UpgradingBuilding>> mUpgradingBuildings = new MutableLiveData<>();
    private MutableLiveData<ArrayList<String>> mNewAvaiableBuildings = new MutableLiveData<>();

    private BuildingsRepository mBuildingRepository;
    private UpgradingRepository mUpgradingRepository;
    private BuildingUpgradeRepository mBuildingUpgradeRepository;
    private AvailableNewBuildingsRepository mNewBuildingsRepository;
    private Timer timer = new Timer();


    /**
     * Observe any "latest page". Remember to stick this observer in all repositories that will receive
     * a new page, and remove it in onCleared method.
     */
    private Observer<Document> latestPageObserver = new Observer<Document>() {
        @Override
        public void onChanged(Document document) {
            VillageResources village = ResourcesRepository.updateResources(document);
            mVillageResources.setValue(village);
            updateUpgradingList(document);
            updateBuildingList(document);
        }
    };


    /**
     * You may call it a Linked List, but I can call my Queue whatever I want, and nobody's gonna
     * complain anything about it, okay?
     * <p>
     * Just kidding. There's no easy way to implement real {@link Queue} here without wasting quite
     * a huge amount of time customizing one (implementing {@link Queue}, then implementing
     * {@link Parcelable} again). Just remember to treat it like a Queue, not a Linked List
     */
    private BuildingQueue queuingBuildings = new BuildingQueue();


    public MainViewModel(@NonNull Application application) {
        super(application);

        mBuildingRepository = BuildingsRepository.getInstance();
        mUpgradingRepository = UpgradingRepository.getInstance();
        mBuildingUpgradeRepository = new BuildingUpgradeRepository();
        mNewBuildingsRepository = new AvailableNewBuildingsRepository();

        // Remember to remove these observer when close
        mBuildingRepository.latestPage().observeForever(latestPageObserver);
        mBuildingUpgradeRepository.latestPage().observeForever(latestPageObserver);
        mNewBuildingsRepository.latestPage().observeForever(latestPageObserver);

        login();
    }


    public void login() {
        SessionRepository.login();
    }


    /**
     * Update building list after requesting a new Jsoup {@link Document}
     */
    public void updateBuildingList() {
        // Request & update inner building list. Inner building MUST always be scraped before outer ones
        mBuildingRepository.updateInnerBuildingList();
        mInnerBuildings = mBuildingRepository.getInnerBuildings();

        // Request & update outer building list
        mBuildingRepository.updateOuterBuildingList();
        mOuterBuildings = mBuildingRepository.getOuterBuildings();

//        mLatestPage = mBuildingRepository.latestPage();
    }


    /**
     * Update building list after receiving a new Jsoup {@link Document}
     * @param document The newly received {@link Document}
     */
    private void updateBuildingList(Document document) {
        // Scrape & update inner building list. Inner building MUST always be scraped before outer ones
        mBuildingRepository.updateInnerBuildingList(document);
        mInnerBuildings = mBuildingRepository.getInnerBuildings();

        // Scrape & update outer building list
        mBuildingRepository.updateOuterBuildingList(document);
        mOuterBuildings = mBuildingRepository.getOuterBuildings();
    }


    /**
     * Request a list of available {@link Building}s, when choosing an empty building site to construct
     * a new {@link Building}
     *
     * @param buildingSite The empty building site to construct a new building in.
     */
    public void getAvailableNewBuildings(Building buildingSite) {
        if (!buildingSite.isBuildingSite) {
            // TODO: after adding multiple villages, include current village here
            Log.d("BUG","ERROR: " + buildingSite.getUrl() + " is not a building site");
            return;
        }

        mNewBuildingsRepository.getBuildingList(buildingSite);

//        mNewAvaiableBuildings = mNewBuildingsRepository.buildingNames();
    }


    /**
     * Update upgrading buildings with a newly loaded Jsoup {@link Document}
     * @param page Jsoup {@link Document} contains new upgrading buildings info
     */
    private void updateUpgradingList(Document page) {
        mUpgradingBuildings.setValue(mUpgradingRepository.updateUpgradingList(page));
    }


    public LiveData<ArrayList<UpgradingBuilding>> upgradingBuildings() {
        return mUpgradingBuildings;
    }


    public LiveData<ArrayList<Building>> outerBuildings() {
        return mOuterBuildings;
    }


    public LiveData<ArrayList<Building>> innerBuildings() {
        return mInnerBuildings;
    }


    public LiveData<Document> latestPage() {
        return mLatestPage;
    }


    public LiveData<VillageResources> villageResources() {
        return mVillageResources;
    }


    public LiveData<ArrayList<String>> newBuildings() {
        return mNewBuildingsRepository.buildingNames();
    }


    /**
     * Count down all timers each second.
     */
    public void initTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {

                    // Timer count down upgrading buildings' time
                    countDownCurrentUpgrading();

                    // Timer update resources info
                    VillageResources village = mVillageResources.getValue();
                    assert village != null;

                    village.timerUpdate();
                    mVillageResources.postValue(village);

                    // Checking queuing buildings
                    // TODO: write special cases for Romans
                    if (User.tribe != Tribe.Romans) {
                        // If no other buildings were upgrading
                        if (Objects.requireNonNull(mUpgradingBuildings.getValue()).isEmpty()) {
                            // And there were items waiting to be upgraded
                            if (queuingBuildings.size() > 0) {
                                // Decrease count down counter
                                mBuildingUpgradeRepository.timerUpdate();
                                // If countdown reached 0
                                if ((mBuildingUpgradeRepository.getWaitTime() <= 0)) {
                                    // And already double-checked
                                    if (mBuildingUpgradeRepository.isRefreshed()) {
                                        // And not sent a request yet
                                        if (!mBuildingUpgradeRepository.alreadyRequesting()) {
                                            // Make sure no requests will be run in parallel
                                            mBuildingUpgradeRepository.setRequesting(true);
                                            // Upgrade this building
                                            mBuildingUpgradeRepository.upgrade(Objects
                                                        .requireNonNull(queuingBuildings.poll()));
                                        }
//                                        Log.i("debug", "timer < 0");
                                    } else {
                                        Building nextBuilding = queuingBuildings.peekFirst();

                                        // Double check conditions
                                        mBuildingUpgradeRepository.setRefreshed(true);
                                        mBuildingUpgradeRepository.updateWaitingTime(nextBuilding);
                                    }
                                }
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    Log.i("debug", "Null pointer at MainViewModel/timer, presumably " +
                        "just because not received first Document yet");
                }
            }
        },0,1000);
    }


    public void setUpgradingQueue(BuildingQueue queue) {
        // reset BuildingUpgradeRepository count down if queue changed
        if (!queue.isTheSameWith(this.queuingBuildings))
            this.mBuildingUpgradeRepository.setWaitTime(0);

        this.queuingBuildings = queue;
        this.mBuildingUpgradeRepository.setUpgradingQueue(queue);
    }


    public BuildingQueue getUpgradingQueue() {
        return queuingBuildings;
    }


    /**
     * Count down current upgrading buildings timer.
     */
    private void countDownCurrentUpgrading() {
        ArrayList<UpgradingBuilding> upgradingBuildings = mUpgradingBuildings.getValue();
        Stack<Integer> finishedIndex = new Stack<>();

        assert upgradingBuildings != null;

        for (int index = 0; index < upgradingBuildings.size(); index++) {
            UpgradingBuilding building = upgradingBuildings.get(index);
            building.timeLeft--;
            upgradingBuildings.set(index, building);

            // if finished upgrading, mark this building to remove from list later
            if (building.timeLeft <= 0) {
                finishedIndex.add(index);
            }

        }

        // remove all finished buildings from list
        for (int index : finishedIndex) {
            upgradingBuildings.remove(index);
        }

        mUpgradingBuildings.postValue(upgradingBuildings);
    }


    @Override
    public void onCleared() {
        mBuildingUpgradeRepository.latestPage().removeObserver(latestPageObserver);
        mBuildingRepository.latestPage().removeObserver(latestPageObserver);
        mNewBuildingsRepository.latestPage().removeObserver(latestPageObserver);
        super.onCleared();
    }
}
